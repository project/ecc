<?php

namespace Drupal\ecc;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Class EccTypeListBuilder.
 *
 * @package Drupal\ecc
 */
class EccTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'id' => $this->t('ID'),
      'title' => $this->t('Title'),
      'category' => $this->t('Category'),
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\ecc\Entity\EccType $entity */
    $row = [];
    $row['id'] = $entity->id();
    $row['title']['data'] = $entity->toLink(NULL, 'edit-form')->toRenderable();
    $row['category'] = $entity->getCategoryLabel();

    return $row + parent::buildRow($entity);
  }

}
