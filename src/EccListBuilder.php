<?php

namespace Drupal\ecc;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EccListBuilder.
 *
 * @package Drupal\ecc
 */
class EccListBuilder extends EntityListBuilder {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance($container, $entity_type);
    $instance->config = $container->get('config.factory');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'id' => $this->t('ID'),
      'title' => $this->t('Title'),
      'type' => $this->t('Type'),
      'category' => $this->t('Category'),
      'link_to_export' => $this->t('Export URL'),
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\ecc\Entity\EccType $type */
    $type = $entity->get('type')->entity;

    $row = [];
    $row['id'] = $entity->id();
    $row['title']['data'] = [
      'link' => $entity->toLink(NULL, 'edit-form')->toRenderable(),
      'machine_name' => [
        '#markup' => " ( {$entity->get('machine_name')->value} ) ",
      ],
    ];
    $row['type']['data'] = $type->toLink(NULL, 'edit-form')->toRenderable();
    $row['category'] = $type->getCategoryLabel();

    if ($type->isExportable()) {
      $uri = $this->config->get('ecc.config')->get('uri_path');
      $row['link_to_export']['data'] = [
        '#title' => $this->t('Link to export'),
        '#type' => 'link',
        '#url' => Url::fromUserInput($uri . '/' . $entity->get('machine_name')->value, [
          'attributes' => [
            'target' => '_blank',
          ],
        ]),
      ];
    }
    else {
      $row['link_to_export'] = '';
    }

    return $row + parent::buildRow($entity);
  }

}
