<?php

namespace Drupal\ecc\Plugin\rest\resource;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\ecc\Entity\Ecc;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a resource to get ECC entity configs.
 *
 * @RestResource(
 *   id = "ecc_resource",
 *   label = @Translation("ECC Resource"),
 *   uri_paths = {
 *     "canonical" = "{name}"
 *   }
 * )
 */
class EccResource extends ResourceBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The plugin implementation definition.
   *
   * @var array
   */
  protected $pluginDefinition;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $serializer_formats, LoggerInterface $logger, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->configFactory = $config_factory;
    $this->pluginDefinition = $plugin_definition;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('config.factory')
    );
  }

  /**
   * Get fields values by config machine name.
   *
   * @param string $name
   *   Machine name.
   *
   * @return \Drupal\rest\ResourceResponse
   *   Response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function get($name) {
    $entity = Ecc::loadByMachineName($name);

    if (!$entity) {
      throw new NotFoundHttpException($this->t('ECC not found'));
    }

    /** @var \Drupal\ecc\Entity\EccType $type */
    $type = $entity->get('type')->entity;

    // Check if config is exportable.
    if (!$type->isExportable()) {
      throw new NotFoundHttpException($this->t('ECC not exportable'));
    }

    $response = [
      'id' => $entity->id(),
      'name' => $name,
      'configs' => $entity->getValue(),
    ];

    $return = new ResourceResponse($response);
    $return->addCacheableDependency($entity);

    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginDefinition() {
    if ($uri = $this->configFactory->get('ecc.config')->get('uri_path')) {
      $this->pluginDefinition['uri_paths']['canonical'] = $uri . '/{name}';
    }

    return $this->pluginDefinition;
  }

}
