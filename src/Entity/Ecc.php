<?php

namespace Drupal\ecc\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the ECC entity.
 *
 * @ingroup ecc
 *
 * @ContentEntityType(
 *   id = "ecc",
 *   label = @Translation("ECC"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\ecc\EccListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\ecc\Form\EccForm",
 *       "add" = "Drupal\ecc\Form\EccForm",
 *       "edit" = "Drupal\ecc\Form\EccForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "ecc",
 *   admin_permission = "administer ecc entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/ecc/{ecc}",
 *     "add-form" = "/admin/structure/ecc/add",
 *     "edit-form" = "/admin/structure/ecc/{ecc}/edit",
 *     "delete-form" = "/admin/structure/ecc/{ecc}/delete",
 *     "collection" = "/admin/structure/ecc",
 *   }
 * )
 */
class Ecc extends ContentEntityBase {

  /**
   * Load ECC entity by machine name.
   *
   * @param string $name
   *   Machine name of ECC.
   *
   * @return bool|\Drupal\ecc\Entity\Ecc
   *   ECC entity or FALSE when not found.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function loadByMachineName($name) {
    $storage = \Drupal::entityTypeManager()->getStorage('ecc');
    $entities = $storage->loadByProperties([
      'machine_name' => $name,
    ]);

    return $entities ? reset($entities) : FALSE;
  }

  /**
   * Get values of the ECC entity.
   *
   * @param string $key
   *   The key of the specific config property.
   *
   * @return mixed
   *   The values of the ECC entity.
   */
  public function getValue($key = NULL) {
    if ($this->get('configs')->isEmpty()) {
      return [];
    }

    $values = $this->get('configs')->getValue()[0];

    return !is_null($key) ? $values[$key] ?? [] : $values;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRequired(TRUE);

    $fields['machine_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Machine name'))
      ->setRequired(TRUE);

    $fields['type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Type'))
      ->setSetting('target_type', 'ecc_type')
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);

    $fields['configs'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Configs'));

    return $fields;
  }

}
