<?php

namespace Drupal\ecc\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Serialization\Yaml;

/**
 * Defines the ECC type entity.
 *
 * @ConfigEntityType(
 *   id = "ecc_type",
 *   label = @Translation("ECC type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\ecc\EccTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\ecc\Form\EccTypeForm",
 *       "edit" = "Drupal\ecc\Form\EccTypeForm",
 *       "delete" = "Drupal\ecc\Form\EccTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "ecc_type",
 *   admin_permission = "administer ecc type entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/ecc/type/{ecc_type}",
 *     "add-form" = "/admin/structure/ecc/type/add",
 *     "edit-form" = "/admin/structure/ecc/type/{ecc_type}/edit",
 *     "delete-form" = "/admin/structure/ecc/type/{ecc_type}/delete",
 *     "collection" = "/admin/structure/ecc/type",
 *   }
 * )
 */
class EccType extends ConfigEntityBase {

  /**
   * The Ecc type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Ecc type title.
   *
   * @var string
   */
  protected $title;

  /**
   * The Ecc type category.
   *
   * @var string
   */
  protected $category;

  /**
   * Does Ecc type exportable.
   *
   * @var bool
   */
  protected $exportable;

  /**
   * The Ecc type fields_schema.
   *
   * @var string
   */
  protected $fields_schema;

  /**
   * Get form fields in array format.
   *
   * @return array
   *   Form fields.
   */
  public function getFormFields() {
    $fields_schema = $this->get('fields_schema');
    $fields_schema_decoded = Yaml::decode($fields_schema);

    return $fields_schema_decoded ?? [];
  }

  /**
   * Get category label.
   *
   * @return string
   *   Category label.
   */
  public function getCategoryLabel() {
    return $this->get('category');
  }

  /**
   * Define whether type is exportable.
   *
   * @return bool
   *   Exportable.
   */
  public function isExportable() {
    return $this->get('exportable');
  }

  /**
   * Validate form fields Yaml schema value.
   *
   * @return bool
   *   Validation result.
   */
  public function validateFormFields() {
    $form_fields = $this->getFormFields();

    // We possibly can get a string instead of array.
    return !empty($form_fields) && is_array($form_fields);
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    if (!$this->validateFormFields()) {
      throw new \Exception('Fields schema value is not valid.');
    }

    // Create term if it doesn't exist.
    $this->preSaveCategory();
  }

  /**
   * Save category value.
   *
   * We are using taxonomy term reference for category field and we have to
   * ensure that referenced term is already exist. If not exist we will create a
   * new one.
   */
  protected function preSaveCategory() {
    $category = $this->getCategoryLabel();
    $term_storage = $this->entityTypeManager()->getStorage('taxonomy_term');
    $vid = 'ecc_category';
    $terms = $term_storage->loadByProperties([
      'name' => $category,
      'vid' => $vid,
    ]);

    if (!$terms) {
      $term = $term_storage->create([
        'name' => $category,
        'vid' => $vid,
      ]);
      $term->save();
    }
  }

}
