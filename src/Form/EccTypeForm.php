<?php

namespace Drupal\ecc\Form;

use Drupal\Component\Serialization\Exception\InvalidDataTypeException;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class EccTypeForm.
 *
 * @package Drupal\ecc\Form
 */
class EccTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $vocabulary = $this->entityTypeManager
      ->getStorage('taxonomy_vocabulary')
      ->load('ecc_category');

    if (!$vocabulary) {
      $this->messenger()->addError($this->t("The 'ecc_category' vocabulary not found."));
      return $form;
    }

    /** @var \Drupal\ecc\Entity\EccType $entity */
    $entity = $this->getEntity();

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Config type'),
      '#required' => TRUE,
      '#default_value' => $entity->label(),
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\ecc\Entity\EccType::load',
        'source' => ['title'],
      ],
      '#disabled' => !$entity->isNew(),
    ];

    $form['category'] = [
      '#type' => 'select',
      '#title' => $this->t('Category'),
      '#required' => TRUE,
      '#options' => $this->getConfigCategoryOptions(),
      '#default_value' => $entity->get('category'),
      '#description' => $this->t('To manage categories follow this @link.', [
        '@link' => $vocabulary->toLink($this->t('link'), 'overview-form')->toString(),
      ]),
      '#disabled' => !$entity->isNew(),
    ];

    $form['category']['#options']['add_new_category'] = $this->t('- Add new category -');
    $form['new_category'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Add new category'),
      '#states' => [
        'visible' => [
          ':input[name="category"]' => ['value' => 'add_new_category'],
        ],
      ],
    ];

    $form['exportable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Is exportable?'),
      '#description' => $this->t('Enable this option to allow exporting of configs via REST API.'),
      '#default_value' => $entity->get('exportable') ?? 1,
    ];

    $form['fields_schema'] = [
      '#type' => 'textarea',
      '#required' => TRUE,
      '#title' => $this->t('Fields schema'),
      '#attributes' => ['data-yaml-editor' => 'true'],
      '#default_value' => $entity->get('fields_schema'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $entity = $this->getEntity();
    try {
      $valid = $entity->validateFormFields();
    }
    catch (InvalidDataTypeException $exception) {
      $form_state->setErrorByName('fields_schema', $this->t('Fields schema value is not valid: @message', ['@message' => $exception->getMessage()]));
      return;
    }
    if (!$valid) {
      $form_state->setErrorByName('fields_schema', $this->t('Fields schema value is not valid'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $new_category = $form_state->getValue('new_category');
    if (!empty($new_category) && $form_state->getValue('category') === 'add_new_category') {
      $entity->set('category', $new_category);
    }
    $status = parent::save($form, $form_state);

    $t_args = ['%name' => $entity->label()];

    if ($status === SAVED_UPDATED) {
      $this->messenger()->addStatus($this->t('The config type %name has been updated.', $t_args));
    }
    elseif ($status === SAVED_NEW) {
      $this->messenger()->addStatus($this->t('The config type %name has been added.', $t_args));
    }

    $form_state->setRedirect('entity.ecc_type.collection');
  }

  /**
   * Get config category options.
   *
   * @return array
   *   Array of options.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getConfigCategoryOptions() {
    $storage = $this->entityTypeManager->getStorage('taxonomy_term');
    $terms = $storage->loadByProperties([
      'vid' => 'ecc_category',
    ]);

    $options = [];

    foreach ($terms as $term) {
      $options[$term->label()] = $term->label();
    }

    return $options;
  }

}
