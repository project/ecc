<?php

namespace Drupal\ecc\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigurationForm.
 *
 * @package Drupal\ecc\Form
 */
class ConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ecc.config');

    $form['uri_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Source'),
      '#description' => $this->t('Uri path for the REST resource. Example: /api/v1/config.'),
      '#default_value' => $config->get('uri_path'),
      '#required' => TRUE,
      '#maxlength' => 55,
      '#size' => 60,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('ecc.config')
      ->set('uri_path', '/' . trim($form_state->getValue('uri_path'), '/'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ecc.config'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ecc_configuration_form';
  }

}
