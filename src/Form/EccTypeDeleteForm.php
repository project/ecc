<?php

namespace Drupal\ecc\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Builds the form to delete Ecc type entities.
 */
class EccTypeDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %name ECC Type?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $type = $this->entity->id();
    $count = count($this->getEccEntities($type));
    if ($count) {
      $message = $this->formatPlural($count,
        'Warning! There is 1 ECC entity of %type type. This entity will also be deleted and this action cannot be undone.',
        'Warning! There are %count ECC entities of %type type. These entities will also be deleted and this action cannot be undone.',
        ['%type' => $type, '%count' => $count]
      );
    }
    else {
      $message = $this->t('This action cannot be undone.');
    }
    return $message;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.ecc_type.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $entity->delete();

    $this->messenger()->addMessage(
      $this->t('ECC type @label was deleted.', [
        '@label' => $entity->label(),
      ])
    );

    $form_state->setRedirectUrl($this->getCancelUrl());

    // Delete all ecc entities of this type too. Create batch for deleting.
    $ecc_entities = $this->getEccEntities($entity->id());
    if (empty($ecc_entities)) {
      return;
    }
    $batch = [
      'title' => $this->t('Deleting ECC entities of %type type.', ['%type' => $entity->id()]),
      'init_message' => $this->t('Initializing.'),
      'progress_message' => $this->t('Completed @current of @total.'),
      'error_message' => $this->t('An error has occurred.'),
    ];
    $chunks = array_chunk($ecc_entities, 2);
    $operations = [];
    foreach ($chunks as $chunk) {
      $operations[] = [
        [$this, 'processBatch'],
        [$chunk],
      ];
    }
    $batch['operations'] = $operations;
    batch_set($batch);
  }

  /**
   * Get number of ecc entities of given type.
   *
   * @param string $type
   *   Ecc type entity machine name.
   *
   * @return array
   *   List of ecc_entities.
   */
  protected function getEccEntities($type) {
    $query = $this->entityTypeManager->getStorage('ecc')->getQuery();
    $query->condition('type', $type);
    return $query->execute();
  }

  /**
   * Processes batch. Delete ecc entities.
   *
   * @param array $entity_ids
   *   List of entity ids to delete.
   * @param array $context
   *   The batch context.
   */
  public static function processBatch(array $entity_ids, array &$context) {
    $ecc_storage = \Drupal::entityTypeManager()->getStorage('ecc');
    $entities = $ecc_storage->loadMultiple($entity_ids);
    $ecc_storage->delete($entities);
    $context['results']['deleted'] += count($entity_ids);
    $context['message'] = t('Deleted @count entities', ['@count' => $context['results']['deleted']]);
  }

}
