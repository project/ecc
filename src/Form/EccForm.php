<?php

namespace Drupal\ecc\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\ecc\Entity\Ecc;
use Drupal\ecc\Entity\EccType;

/**
 * Class EccForm.
 *
 * @package Drupal\ecc\Form
 */
class EccForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $entity = $this->getEntity();

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Config name'),
      '#required' => TRUE,
      '#default_value' => $entity->label(),
    ];
    $form['machine_name'] = [
      '#type' => 'machine_name',
      '#required' => TRUE,
      '#default_value' => $entity->get('machine_name')->value,
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'source' => ['label'],
      ],
    ];
    $form['type'] = [
      '#type' => 'select',
      '#description' => $this->t('See list of available types <a href="/admin/structure/ecc/type">here</a>'),
      '#title' => $this->t('Type'),
      '#options' => $this->getConfigTypeOptions(),
      '#required' => TRUE,
      '#default_value' => $entity->get('type')->target_id,
      '#ajax' => [
        'callback' => [$this, 'onConfigTypeChange'],
        'wrapper' => 'subform-wrapper',
      ],
      '#disabled' => !$entity->isNew(),
    ];
    $form['configs'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Subform'),
      '#tree' => TRUE,
      '#attributes' => [
        'id' => 'subform-wrapper',
      ],
    ];

    $type_id = $form_state->getValue('type') ?? $entity->get('type')->target_id;

    if (!is_null($type_id)) {
      $config_type = $this->entityTypeManager
        ->getStorage('ecc_type')
        ->load($type_id);

      $form['configs'] += $this->buildSubForm($config_type);
    }
    else {
      $form['configs']['#description'] = $this->t('Select config type to build a subform.');
    }

    return $form;
  }

  /**
   * Recursive method to build fields with deep.
   *
   * @param array $fields
   *   Fields schema.
   * @param array $default_values
   *   Default values.
   *
   * @return array
   *   Form with all generated fields.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function buildFields(array $fields, array $default_values) {
    $form = [];

    foreach ($fields as $field_name => $field_info) {
      $form[$field_name] = [
        '#type' => $field_info['type'],
        '#title' => $field_info['title'],
        '#required' => $field_info['required'] ?? FALSE,
        '#description' => $field_info['description'] ?? '',
      ];

      // Provide default value.
      if (isset($default_values[$field_name])) {
        $form[$field_name]['#default_value'] = $default_values[$field_name];
      }

      // Handle managed file field.
      if ($field_info['type'] === 'managed_file' && isset($field_info['upload_location'])) {
        $form[$field_name]['#element_validate'] = [[$this, 'validateManagedFile']];
        $form[$field_name]['#default_value'] = isset($default_values[$field_name]['id']) ? [
          $default_values[$field_name]['id'],
        ] : NULL;

        // Use default file scheme.
        $default_scheme = $this->config('system.file')->get('default_scheme');
        $form[$field_name]['#upload_location'] = $default_scheme . '://' . $field_info['upload_location'];
      }

      // Handle entity autocomplete field.
      if ($field_info['type'] === 'entity_autocomplete') {
        $form[$field_name]['#validate_reference'] = FALSE;
        $form[$field_name]['#target_type'] = $field_info['target_type'] ?? 'node';

        // Handle dynamic entity storage for selected entity type.
        $entity_type_storage = $this->entityTypeManager
          ->getStorage($form[$field_name]['#target_type']);

        $form[$field_name]['#default_value'] = !empty($default_values[$field_name]) ? $entity_type_storage->load($default_values[$field_name]) : NULL;

        if (isset($field_info['bundles'])) {
          $form[$field_name]['#selection_settings'] = ['target_bundles' => $field_info['bundles']];
        }
      }

      // Provide options for select, radios.
      if (isset($field_info['options'])) {
        $form[$field_name]['#options'] = $field_info['options'];
      }

      // Provide min value for number.
      if (isset($field_info['min'])) {
        $form[$field_name]['#min'] = $field_info['min'];
      }

      // Provide max value for number.
      if (isset($field_info['max'])) {
        $form[$field_name]['#max'] = $field_info['max'];
      }

      // Provide step value for number.
      if (isset($field_info['step'])) {
        $form[$field_name]['#step'] = $field_info['step'];
      }

      // Provide tree option for parent field.
      if (isset($field_info['children'])) {
        $form[$field_name]['#tree'] = TRUE;
        unset($form[$field_name]['#default_value']);
        $children_values = $default_values[$field_name] ?? [];

        $form[$field_name] += $this->buildFields($field_info['children'], $children_values);
      }
    }

    return $form;
  }

  /**
   * Validate managed file element.
   *
   * @param array $element
   *   File element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function validateManagedFile(array $element, FormStateInterface $form_state) {
    // Skip ajax callback.
    if (isset($_GET['ajax_form'])) {
      return;
    }

    if (empty($element['#files'])) {
      $form_state->setValueForElement($element, NULL);
      return;
    }

    /** @var \Drupal\file\Entity\File $file */
    $file = reset($element['#files']);
    $file->setPermanent();
    $file->save();

    $uri = $file->getFileUri();

    $form_state->setValueForElement($element, [
      'id' => $file->id(),
      'url' => file_create_url($uri),
    ]);
  }

  /**
   * Build sub form for ECC config.
   *
   * @param \Drupal\ecc\Entity\EccType $config_type
   *   Config type.
   *
   * @return array
   *   Sub form.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function buildSubForm(EccType $config_type) {
    $fields = $config_type->getFormFields();

    /** @var \Drupal\ecc\Entity\Ecc $entity */
    $entity = $this->getEntity();
    $values = $entity->getValue();

    return $this->buildFields($fields, $values);
  }

  /**
   * Ajax callback.
   */
  public function onConfigTypeChange(array $form, FormStateInterface $form_state) {
    return $form['configs'];
  }

  /**
   * Get config type options.
   *
   * @return array
   *   Array with options.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getConfigTypeOptions() {
    $types = $this->entityTypeManager
      ->getStorage('ecc_type')
      ->loadMultiple();

    $options = [];

    foreach ($types as $type) {
      $options[$type->id()] = $type->label();
    }

    return $options;
  }

  /**
   * Check if entity is already exist.
   *
   * @param string $name
   *   Entered machine name.
   *
   * @return bool
   *   True if entity exists.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function exists($name) {
    $entity = Ecc::loadByMachineName($name);

    return !empty($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = parent::save($form, $form_state);
    $entity = $this->getEntity();

    $t_args = ['%name' => $entity->label()];

    if ($status === SAVED_UPDATED) {
      $this->messenger()->addStatus($this->t('The config %name has been updated.', $t_args));
    }
    elseif ($status === SAVED_NEW) {
      $this->messenger()->addStatus($this->t('The config %name has been added.', $t_args));
    }

    $form_state->setRedirect('entity.ecc.collection');
  }

}
