INTRODUCTION
------------------

The Editable Custom Configuration (ECC) gives you ability to create any configuration types, content, or categories with any fields schema through the UI on the fly. The ECC module can help you with the following:

* Creation and management of configuration types with a field schema (with the help of the YAML editor)
* Adding multiple configuration content based on configuration type
* Loading configuration content by machine name and getting its values
* Using it in the code to fetch configuration
* Exporting configuration via REST to use it for decoupled apps
* Using a dynamic field schema based on YAML editor


 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/ecc

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/ecc

REQUIREMENTS
------------------

The Editable Custom Configuration (ECC) requires the following modules:

 * YAML Editor (https://www.drupal.org/project/yaml_editor)
 * RESTful Web Services (needs to be enabled in the Drupal core)
 * Taxonomy (is enabled by default in the Drupal core)
 
RECOMMENDED MODULES
------------------------------
REST UI (https://www.drupal.org/project/restui): when installed, it provides a user interface for editing your entity configuration.

INSTALLATION
----------------
* Installation via Composer is recommended by the maintainer, because the module has dependancies. If you use a standard way of installing the module, be sure to check out the Requirements section of this Readme file.

CONFIGURATION
-------------------
The configuration form of the ECC module is found at /admin/config/ecc/settings. You can export entities in JSON with the help of the RESTful Web Services. The configuration form gives you the flexible ways to configure the export path (for example, /API/V1/config/[config-machine-name]). 

FAQ
------------------------------

Q: How to use the export?
 
A:  You need to install and enable the REST UI contributed module (see the Recommended modules section of this Readme file). Using its UI, enable the ECC resource , go to the ECC Entities page and click "Link to export". 

Q: Which types of fields can I use in a fields schema?

A: You can use most of the fields available in the Form API:

* managed_file
* entity_autocomplete
* fieldset
* textfield
* select
* chekboxes
* radius
* number

See more types of fields in the module's EccForm.php file at ECC Forms — Build fields.

DOCUMENTATION
-------------------
To give you more information about the module, we are in progress of preparing the detailed documentation page. Please check for it later.
